import firebase from 'firebase';

firebase.initializeApp({    
    apiKey: "AIzaSyCAPqfYZ2qiZubP7XijKWpKScqclbzo8C8",
    authDomain: "my-doctor-01-9f292.firebaseapp.com",
    databaseURL: "https://my-doctor-01-9f292.firebaseio.com",
    projectId: "my-doctor-01-9f292",
    storageBucket: "my-doctor-01-9f292.appspot.com",
    messagingSenderId: "256004661778",
    appId: "1:256004661778:web:e72ea0b1e9c2618e23aecb"     
});
const Fire = firebase;
export default Fire;