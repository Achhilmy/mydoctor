import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { colors, fonts } from '../../../utils';

const Link = ({title, size, align, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.text(size, align)}>{title}</Text>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    text: (size, align) => ({
        fontSize: size, 
        color: colors.text.secondary,
        fontFamily:fonts.primary[400],
        textDecorationLine:'underline',
        textAlign: align

    }),
});

export default Link;
