import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { IconDoctor, 
        IconHospital, 
        IconMessage, 
        IconDoctorActive, 
        IconMessageActive, 
        IconHospitalActive} from '../../../assets'
import { fonts, colors } from '../../../utils';
 ''

const TabItem = ({title, active, onPress, onLongPress}) => {
    const Icon = () => {
        if (title === 'Doctor'){
            return active ? <IconDoctorActive /> : <IconDoctor />;
        }
        if (title === 'Message'){
            return active ? <IconMessageActive /> : <IconMessage />
        }
        if (title === 'Hospital'){
            return active ? <IconHospitalActive /> : <IconHospital />
        }
        return <IconDoctor />;
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
            <Icon /> 
            <Text style={styles.text(active)}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container:{
        alignItems:'center',

    },
    text: (active)=>({
        fontSize: 10,
        marginTop:4,
        fontFamily: fonts.primary[600],
        color: active ? colors.text.menuActive : colors.text.menuInactive,
        
    })
})

export default TabItem
