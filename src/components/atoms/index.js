import Button from './Button';
import Gap from './Gap';
import Input from './Input';
import Link from './Link';
import IconProfile from './IconProfile'

export {Button, 
    Gap, 
    Input, 
    Link,
    IconProfile,
} ;