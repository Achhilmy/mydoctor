import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';
import { fonts, colors } from '../../../utils';
import { DummyDoctor3 } from '../../../assets';

const Other = () => {
    return (
        <View style={styles.container}>
            <Image source={DummyDoctor3} style={styles.avatar}/>
            <View>
                <View style={styles.chatContent}>
                    <Text style={styles.text}>Saya hendak tanya, Apakah makan jeruk tiap hari baik ?</Text>
                </View>
                <Text style={styles.date}>4.20</Text>                
            </View>
    </View>
    )
}

export default Other;

const styles = StyleSheet.create({
    container:{
        alignItems: 'flex-end',
        marginBottom: 20,
        paddingLeft: 16,
        borderRadius: 30 / 2,
        flexDirection: 'row'
    },
    avatar:{
        width: 30,
        height: 30,
        marginRight: 12,

    },
    chatContent:{
        padding: 12, 
        paddingRight: 18,
        backgroundColor: colors.primary,
        maxWidth:'80%',
        borderRadius: 10,
        borderBottomLeftRadius: 0

    },
    text:{
        fontSize:14,
        fontFamily: fonts.primary.normal,
        color: colors.white,
    },
    date :{
        fontSize: 11,
        fontFamily: fonts.primary.normal,
        color: colors.text.secondary,
        marginTop: 8,

    }
})

