 import React, {useEffect, useState} from 'react'
 import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import { DummyUser, ILPhotoNull } from '../../../assets'
import { fonts, colors, getData} from '../../../utils';
 
 const HomeProfile = ({onPress}) => {
     const [profile, setProfile] = useState({
         photo: ILPhotoNull,
         fullName:'',
         profession:''
     });
     useEffect(()=>{
        getData('user').then(res=>{
            const data = res;
            data.photo= {uri: res.photo};
            console.log('data profile:', data);
            setProfile(res);
        });
    },[]);
     return (
         <TouchableOpacity style={styles.container} onPress={onPress}>
             <Image source={profile.photo} style={styles.avatar}/>
             <View>
            <Text style={styles.name}>{profile.fullName}</Text>
            <Text style ={styles.profesion}>{profile.profession}</Text>
            </View>
         </TouchableOpacity>
     );
 };
 
 const styles = StyleSheet.create({
     container:{
        flexDirection: 'row', 
     },
     avatar:{
         width: 46,
         height:46,
         borderRadius: 46 / 2,
         marginRight: 12,
     },
     name:{
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        textTransform:'capitalize'
     },
     profesion:{
         fontSize: 12,
         fontFamily: fonts.primary[400],
         color: colors.text.secondary,
         textTransform:'capitalize'
     },
 });
 
 export default HomeProfile;
 