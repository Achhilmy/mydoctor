import React from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import { colors, fonts } from '../../../utils'
import { Button } from '../../atoms'

const InputChat = () => {
    return (
        <View style={styles.container}>
           <TextInput style={styles.input} placeholder='tulis pesan untuk Riska umi'/>
           <Button type='btn-icon-send' />
        </View>
    )
}

export default InputChat

const styles = StyleSheet.create({
    container:{
        padding: 16,
        flexDirection:'row'
    },
    input:{
        padding: 14,
        backgroundColor: colors.disable,
        borderRadius: 10,
        flex: 1,
        marginRight: 10,
        fontSize: 14,
        maxHeight: 45,
        fontFamily: fonts.primary.normal
       
    }
})
