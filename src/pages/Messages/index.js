import React, {useState} from 'react'
import { View, Text, StyleSheet } from 'react-native'
import List from '../../components/molecules/List'
import { colors, fonts } from '../../utils' 
import { DummyDoctor2, DummyDoctor1, DummyDoctor3 } from '../../assets/dummy'

const Message = ({navigation}) => {
    const [doctor, setDoctor] = useState([
        {
            id: 1,
            profile: DummyDoctor3,
            name: 'Riska Umi Faridatus',
            desc: 'Baik Bu, terima kasih banyak atas wakt...'
        },
        {
            id: 2,
            profile: DummyDoctor2,
            name: 'Riska Umi Faridatus',
            desc: 'Baik Bu, terima kasih banyak atas wakt...'
        },
        {
            id: 3,
            profile: DummyDoctor1,
            name: 'Riska Umi Faridatus',
            desc: 'Baik Bu, terima kasih banyak atas wakt...'
        },
    ])
    return (
        <View style={styles.page}>
            <View style={styles.content}>
                <Text style={styles.title}>Messages</Text>
                {doctor.map(doctors=>{
                    return(
                        <List 
                            key={doctors.id}
                            profile={doctors.profile}
                            name={doctors.name}
                            desc={doctors.desc}
                            onPress={()=>navigation.navigate('Chatting')}
                        />

                    )
                })}
              
            </View>
        </View>
    )
}

export default Message

const styles = StyleSheet.create({
    page:{
        backgroundColor: colors.secondary,
        flex: 1
    },
    content:{
        backgroundColor: colors.white,
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    title:{
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginLeft: 16,

    }
})
