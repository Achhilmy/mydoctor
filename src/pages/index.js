import Splash from './Splash';
import GetStarted from './GetStarted';
import Register from './Register';
import Login from './Login';
import UploadPhoto from './UploadPhoto';
import Doctor from './Doctor';
import Hospital from './Hospital';
import Message from './Messages';
import ChooseDoctor from './ChoseDoctor';
import Chatting from './Chatting';
import UserProfile from './UserProfile';
import UpdateProfile from './UpdateProfile';
import DoctorProfile from './DoctorProfile'

export {Splash, 
        GetStarted, 
        Register, 
        Login, 
        UploadPhoto, 
        Doctor, 
        Hospital, 
        Message,
        ChooseDoctor,
        Chatting,
        UserProfile,
        UpdateProfile,
        DoctorProfile,   

    };