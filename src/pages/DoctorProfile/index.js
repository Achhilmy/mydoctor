import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Header, Profile, ProfileItem, Button, Gap } from '../../components'
import { colors } from '../../utils'

const DoctorProfile = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header title='Doctor Profile'  onPress={()=> navigation.goBack()}/>
            <Profile name='Alexa Rachel' desc='Doctor Anak'/>
            <Gap height={10}/>
            <ProfileItem  label='Alumnus' value='Universitas Brawijaya malang'/>
            <ProfileItem label='Tempat Praktik ' value='Bandung, Jawa Barat'/>
            <ProfileItem label='No Str' value='5614515151515151654'/>
            <View style={styles.action}>
                <Button title='Start Consultation' onPress={()=>navigation.navigate('Chatting')}/>

            </View>
        </View>
    )
}

export default DoctorProfile

const styles = StyleSheet.create({
    page:{
        backgroundColor: colors.white,
        flex: 1
    }, action:{
        paddingHorizontal: 40,
        paddingTop: 23,
    }
})
