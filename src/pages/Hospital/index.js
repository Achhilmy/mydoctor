import React from 'react'
import { View, Text, ImageBackground, StyleSheet } from 'react-native'
import { ILHospital } from '../../assets/illustration'
import { fonts, colors } from '../../utils'
import { ListHospital } from '../../components/molecules';
import {DummyHospital1, DummyHospital2, DummyHospital3} from '../../assets'

const Hospital = () => {
    return (
        <View style={styles.page}>
            <ImageBackground source={ILHospital} style={styles.background}>
                <Text style={styles.title}>Nearby Hospital</Text>
                <Text style={styles.desc}>3 Tersedia</Text>
            </ImageBackground>
            <View style={styles.content}>
                <ListHospital type='rumah sakit' name='rumah sakit bhayangkara' address='balowerti kediri' pic={DummyHospital1}/>
                <ListHospital type='rumah sakit' name='rumah sakit bhayangkara' address='balowerti kediri' pic={DummyHospital2}/>
                <ListHospital type='rumah sakit' name='rumah sakit bhayangkara' address='balowerti kediri' pic={DummyHospital3}/>
            </View>
        </View>
    )
}

export default Hospital

const styles = StyleSheet.create ({
    page:{
        backgroundColor: colors.secondary,
        flex: 1
    },
    background:{
        height: 240,
        paddingTop: 30
    },
    title:{
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.white,
        textAlign: 'center'
    },
    desc:{
        fontSize: 14,
        fontFamily: fonts.primary[300], 
        color: colors.white,
        marginTop: 6,
        textAlign: 'center'
    },
    content:{
        backgroundColor: colors.white,
        borderRadius: 20,
        flex: 1,
        marginTop: -30,
        paddingTop: 14

    }
})

