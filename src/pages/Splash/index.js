import React, {useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILLogo } from '../../assets'
import { Fire } from '../../config'
import { fonts } from '../../utils'

const Splash =({navigation}) => {
    useEffect(() => {
        setTimeout(()=>{
            Fire.auth().onAuthStateChanged(user=>{
                if(user){
                    //userLogin
                    console.log('user:', user);
                    navigation.replace('MainApp');
                }else{
                    //userLogout
                    navigation.replace('GetStarted')
                }
            });          
        },3000)
    },[navigation])
    return (
        <View style={styles.pages}>
           <ILLogo />
           <Text style={styles.title}>My Doctor</Text>
        </View>
    )
}

export default Splash;

const styles = StyleSheet.create({
    pages: {
        backgroundColor: 'white',
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    title:{
        fontSize: 20,
        fontWeight:'200',
        fontFamily: fonts.primary[400],
        color:'#112340',
        marginTop: 20
    }
})
