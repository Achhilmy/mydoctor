import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Header, List } from '../../components'
import { 
        DummyDoctor1, 
        DummyDoctor2, 
        DummyDoctor3, 
        DummyDoctor4,
        DummyDoctor5,
        DummyDoctor6,} from '../../assets'
import { colors } from '../../utils'

const ChoseDoctor = ({navigation}) => { 
    return (
        <View style={styles.page}>
            <Header 
                type='dark' 
                title='Pilih Dokter' 
                onPress={()=> navigation.goBack()} />
            <List
                type='next' 
                profile={DummyDoctor1} 
                name='riska umi' 
                desc='dokter anak'
                onPress={()=> navigation.navigate('Chatting')}/>
            <List
                type='next' 
                profile={DummyDoctor2} 
                name='riska umi' 
                desc='dokter anak'
                onPress={()=> navigation.navigate('Chatting')}/>
            <List
                type='next' 
                profile={DummyDoctor3} 
                name='riska umi' 
                desc='dokter anak'
                onPress={()=> navigation.navigate('Chatting')}/>
            <List
                type='next' 
                profile={DummyDoctor4} 
                name='riska umi'
                 desc='dokter anak'
                 onPress={()=> navigation.navigate('Chatting')}/>
            <List 
                type='next' 
                profile={DummyDoctor5} 
                name='riska umi' 
                desc='dokter anak'
                onPress={()=> navigation.navigate('Chatting')}/>
            <List
                type='next' 
                profile={DummyDoctor6} 
                name='riska umi'
                desc='dokter anak'
                onPress={()=> navigation.navigate('Chatting')}/>
        </View>
    )
}

export default ChoseDoctor

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1
    }
})
