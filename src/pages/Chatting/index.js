import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Header, ChatItem, InputChat } from '../../components'
import { fonts, colors } from '../../utils'

const Chatting = ({navigation}) => {
    return (
        <View style={styles.page}>
            <Header type='dark-profile' title='Narumi taiga'  onPress={()=> navigation.goBack()}/>
            <Text style={styles.chatDate}>Senin, 27 Maret, 2020</Text>
            <View style={styles.container}>
                <ChatItem isMe />
                <ChatItem />
                <ChatItem isMe/>
            </View>
            <InputChat />
        </View>
    )
}

export default Chatting

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    page:{
        backgroundColor: colors.white,
        flex: 1
    },
    chatDate:{
        fontSize: 11,
        fontFamily: fonts.primary.normal,
        color: colors.text.subTitle,
        textAlign:'center'
    }
})
