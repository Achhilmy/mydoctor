import React, {useEffect} from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import { HomeProfile, DoctorCategory, RatedDoctor, NewsItem, Gap } from '../../components';
import { fonts, colors, getData } from '../../utils';
import {JSONCategoryDoctor, DummyDoctor2, DummyDoctor3, DummyDoctor4} from '../../assets';


const Doctor = ({navigation}) => {    
    return (

        <View style={styles.page}>
            <View style={styles.content}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.wrapperSection}>
                    <Gap height={30}/>
                    <HomeProfile onPress={()=>navigation.navigate('UserProfile')} />
                    <Text style={styles.welcome}>Mau Konsultasi</Text>
                </View>
                <View style={styles.wrapperScroll}>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        <View style={styles.category}>
                            <Gap width={32}/>
                                {/* <DoctorCategory category="dokter umum"/>
                                <DoctorCategory category="psikiater"/>
                                <DoctorCategory category="dokter obat"/>
                                <DoctorCategory category="dokter umum"/>     */}

                                {JSONCategoryDoctor.data.map(item =>{
                                    return (<DoctorCategory key={item.id} 
                                        category={item.category} 
                                        onPress={()=>navigation.navigate('ChooseDoctor')}/>
                                    );
                                })}
                            <Gap width={22}/>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.wrapperSection}>
                    <Text style={styles.sectionLabel}>Top Reted Doctors</Text>
                    <RatedDoctor name='Alexa Reachel' desc='Ortopedi' avatar={DummyDoctor2} onPress={onPress=> navigation.navigate('DoctorProfile')}/>
                    <RatedDoctor name='Sunny Frank' desc='Ortopedi' avatar={DummyDoctor3} onPress={onPress=> navigation.navigate('DoctorProfile')}/>
                    <RatedDoctor name='Fox Wagen' desc='Ortopedi' avatar={DummyDoctor4} onPress={onPress=> navigation.navigate('DoctorProfile')}/>
                    <Text style={styles.sectionLabel}>Good news</Text>

                </View>
                <NewsItem /> 
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <NewsItem />
                <Gap height={30}/>
                </ScrollView>                
            </View>
        </View>
    )
}
export default Doctor;

const styles = StyleSheet.create({
    page:{
       
        backgroundColor: colors.white,
        flex: 1
    },
    welcome :{
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginBottom:16,
        maxWidth: 209
    },
    wrapperSection:{
        paddingHorizontal: 16
    },
    category:{
        flexDirection: 'row'
    },
    wrapperScroll:{
        marginHorizontal:-16
    },
    content:{
        backgroundColor: colors.white,       
        flex: 1,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius:20,
    },
    sectionLabel:{
        fontSize: 16,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 30,
        marginBottom: 16
    }
});
