import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ILPhotoNull } from '../../assets';
import { Header, Profile, List, Gap } from '../../components'
import { Fire } from '../../config';
import { getData } from '../../utils';
import { showMessage} from 'react-native-flash-message';

const UserProfile = ({navigation}) => {
    const [profile, setProfile] = useState({
        fullName :'',
        profession:'',
        photo:ILPhotoNull,
    });
    useEffect(()=>{
        getData('user').then(res=>{
            const data = res;
            data.photo = {uri: res.photo};
            setProfile(data);
        });
    })

    const signOut = () =>{
        Fire.auth().signOut().then(()=>{
            console.log('success sign out');
            navigation.replace('GetStarted');
        }).catch(err =>{
            showMessage({
                message: err.message,
                type:'default',
                backgroundColor:colors.error,
                color: 'white' 

            })
        })
    }

    return (
        <View style={styles.page}>
            <Header title='Profile' onPress={()=> navigation.goBack()}/>
            <Gap height={10}/>
            {profile.fullName.length > 0 && (
                <Profile name={profile.fullName} desc={profile.profession} photo={profile.photo}/>
            )}            
            <Gap height={14}/>
            <List 
                    name='Edit Profile' 
                    desc='last update yesterday' 
                    type='next' 
                    icon='edit-profile' 
                    onPress={onPress=>navigation.navigate('UpdateProfile')}/>
            <List 
                    name='Languange' 
                    desc='last update yesterday' 
                    type='next' 
                    icon='languange'/>
            <List 
                    name='Give Us Rate' 
                    desc='last update yesterday' 
                    type='next' 
                    icon='rate'/>
            <List 
                    name='Sign Out' 
                    desc='last update yesterday' 
                    type='next' 
                    icon='help'
                    onPress={signOut}
                    />
        </View>
    )
}

export default UserProfile

const styles = StyleSheet.create({
    page:{
        flex: 1,
        backgroundColor:'white'
    }
})
