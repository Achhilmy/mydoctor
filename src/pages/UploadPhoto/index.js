import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ImagePickerIOS } from 'react-native';
import { Header, Button, Link, Gap } from '../../components';
import { ILPhotoNull, IconAdd, IconRemove } from '../../assets';
import {colors, fonts, storeData} from '../../utils';
import ImagePicker from 'react-native-image-picker';
import { showMessage} from 'react-native-flash-message'
import Fire from '../../config/Fire';
const UpladPhoto = ({navigation, route}) => {
    const {fullName, profession, uid} = route.params;
    const [photoForDB, setPhotoForDB] = useState('');
    const [hasPhoto, setHasPhoto] = useState(false);
    const [photo, setPhoto] = useState(ILPhotoNull);

    const getImage = () =>{
        ImagePicker.launchImageLibrary({
            quality:0.5, maxWidth: 200, maxHeight: 200
        },response => {           
            console.log('response', response)
            if(response.didCancel || response.error){
                showMessage({
                message:'oops kayaknya fotonyta tidak ada',
                type:'default',
                backgroundColor: colors.error,
                color:colors.white

                });
            } else {
                console.log('response getImage:' , response)
                const source = {uri: response.uri};

                setPhotoForDB(`data: ${response.type};base64, ${response.data}`);               
                setPhoto(source);
                setHasPhoto(true);
              }
            },
            );
    };
    const uploadAndContinue = () =>{
        Fire.database()
        .ref('users/' + uid + '/')
        .update({photo: photoForDB });

        const data = route.params;
        data.photo = photoForDB;

        storeData('user', data);

        navigation.navigate('MainApp')
    }
    return (
        <View style={styles.page}>
            <Header title='Upload Photo'/>
            <View style={styles.content}>
                <View style={styles.profile}>
                    <TouchableOpacity style={styles.avatarWrapper}onPress={getImage}>
                        <Image source={photo} style={styles.avatar} />
                        {hasPhoto &&  <IconRemove style={styles.addPhoto} /> }
                        {!hasPhoto && <IconAdd style={styles.addPhoto}/>}                      
                    </TouchableOpacity>
                    <Text style={styles.name}>{fullName}</Text>
                    <Text style={styles.profesion}>{profession}</Text>
                </View>
                <View>
                    <Button 
                    disable={!hasPhoto}
                        title='Upload and Continue'
                        onPress={uploadAndContinue}/>
                    <Gap height={30}/>
                    <Link title='skip for this' align='center'size={16} onPress={()=>navigation.navigate('MainApp')}/>
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    page:{
        flex:1,
        backgroundColor: colors.white
    },
    content:{
        paddingHorizontal: 40,
        paddingBottom:40,
        flex: 1,
        justifyContent: 'space-between'

    },
    profile:{
        alignItems:'center',
        flex:1,
        justifyContent: 'center'
    },
    avatar :{
        width: 110,
        height: 110,
        borderRadius: 110 / 2
    },
    avatarWrapper:{
        width: 130,
        height: 130,
        borderWidth: 1,
        borderColor: colors.border,
        borderRadius: 130 / 2,
        alignItems: 'center',
        justifyContent:'center',

    },
    addPhoto:{
        position: 'absolute',
        bottom: 8,
        right: 6
    },
    name:{
        fontSize: 24,
        color: colors.text.primary,
        textAlign: 'center',
        fontFamily: fonts.primary[600]
    },
    profesion:{
        fontSize:18,
        fontFamily: fonts.primary.normal,
        textAlign:'center',
        color: colors.text.secondary,
        marginTop: 4,
    },
})
export default UpladPhoto;
