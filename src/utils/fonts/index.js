export const fonts ={
    primary :{
        200:'Monserrat-ExtraLight',
        300:'Monserrat-light',
        400:'Monserrat-Regular',
        600:'Monserrat-SemiBold',
        700:'Monserrat-Bold',
        800:'Monserrat-ExtraBold',
        900:'Monserrat-Black',
        normal:'Monserrat-Regular'
    },
};