import {useState} from 'react';

export const useForm = initialValue => {
    const [values, setValues] = useState(initialValue);
    return [
        values,
        (fromType, formValue) => {
            if(fromType==='reset'){
                return setValues(initialValue);
            }
            return setValues({...values, [fromType]: formValue});
        },
    ];
};