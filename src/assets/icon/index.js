import IconBackDark from './arrow_back_24px.svg';
import IconAdd from './ic_add_photo.svg';
import IconRemove from './ic_remove_photo.svg';
import IconDoctor from './ic_doctor.svg';
import IconMessage from './ic_message.svg';
import IconHospital from './ic_hospital.svg';
import IconDoctorActive from './ic_doctor_active.svg';
import IconMessageActive from './ic_message_active.svg';
import IconHospitalActive from './ic_hospital_active.svg';
import IconStar from './ic_star.svg';
import IconNext from './ic_right.svg';
import IconBackLight from './ic_backLight.svg';
import IconSendDark from './ic_send_dark.svg';
import IconSendLight from './ic_send_light.svg';
import IconRate from './ic_rate.svg';
import IconHelp from './ic_Help.svg';
import IconLanguange from './ic_languange.svg';
import IconEditProfile from './ic_editProfile.svg';


export {
        IconBackDark, 
        IconAdd, 
        IconRemove,
        IconDoctor,
        IconHospital,
        IconMessage,
        IconDoctorActive,
        IconHospitalActive,
        IconMessageActive,
        IconStar,       
        IconNext, 
        IconBackLight,
        IconSendDark,
        IconSendLight,
        IconRate,
        IconHelp,
        IconLanguange,
        IconEditProfile,
    };