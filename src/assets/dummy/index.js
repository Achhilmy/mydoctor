import DummyUser from './user.png';
import DummyDoctor1 from './doc1.png';
import DummyDoctor2 from './doc2.png';
import DummyDoctor3 from './doc3.png';
import DummyDoctor4 from './doc4.png';
import DummyDoctor5 from './doc5.png';
import DummyDoctor6 from './doc6.png';
import DummyDoctor7 from './doc7.png';
import DummyDoctor8 from './doc8.png';
import DummyNews1 from './news1.png';
import DummyNews2 from './news2.png';
import DummyNews3 from './news3.png';
import DummyHospital1 from './hospital1.png';
import DummyHospital2 from './hospital2.png';
import DummyHospital3 from './hospital3.png';


export {
    DummyUser,
    DummyDoctor1,
    DummyDoctor2,
    DummyDoctor3,
    DummyDoctor4,
    DummyDoctor5,
    DummyDoctor6,
    DummyDoctor7,
    DummyDoctor8,    
    DummyNews1,
    DummyNews2,
    DummyNews3,
    DummyHospital1,
    DummyHospital2,
    DummyHospital3,
};