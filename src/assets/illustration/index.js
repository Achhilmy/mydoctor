import ILLogo from './logo.svg';
import ILGetStarted from './background.png';
import ILPhotoNull from './photo_null.png';
import ILCatUmum from './cat_doc_umum.svg';
import ILCatPsikiater from './cat_doc_psikiater.svg';
import ILCatObat from './cat_doc_obat.svg';
import ILHospital from './background-hospital.png'

export { 
  ILLogo, 
  ILGetStarted, 
  ILPhotoNull,
  ILCatUmum,
  ILCatObat,
  ILCatPsikiater,
  ILHospital,
};
